 class Main {
    public static void main(String[] args) {
        int n = 4; // Number of rows
        int num = 1;

        for (int i = 1; i <= n; i++) {
            // Print leading spaces
            for (int j = 1; j < i; j++) {
                System.out.print("  ");
            }
            // Print numbers
            for (int j = i; j <= n; j++) {
                System.out.print(num++ + " ");
            }
            System.out.println();
        }
    }
}




