class triPattern7 {
    public static void main(String[] args) {
        int rows = 4;
        int count = 4;

        // Outer loop for rows
        for (int i = rows; i >= 1; i--) {
            // Inner loop for columns
            for (int j = 1; j <= i; j++) {
                if (j % 2 == 0) {
                    // Print letters
                    System.out.print((char)('a' + count - j) + " ");
                } else {
                    // Print numbers
                    System.out.print(j + " ");
                }
            }
            System.out.println();
            count--;
        }
    }
}

