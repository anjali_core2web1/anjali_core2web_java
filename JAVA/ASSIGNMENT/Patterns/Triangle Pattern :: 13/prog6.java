 class triPattern {
    public static void main(String[] args) {
        // Loop to print alternating letters and numbers
        for (int i = 1; i <= 3; i++) {
            System.out.print(i + " "); // Print number
            System.out.print((char)('a' + i - 1) + " "); // Print letter
        }
    }
}

