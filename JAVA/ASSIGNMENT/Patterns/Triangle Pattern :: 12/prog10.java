
class triPattern10 {

    public static void main(String[] args) {
        
        char currentChar = 'a';

        for (int i = 1; i <=4; i++) {
		int num=1;
            for (int j = 1; j <=i; j++) {
                if (i % 2 == 1) {
                    System.out.print(num + " ");
		    num++;
                } else {
                    System.out.print(currentChar + " ");
		    currentChar++;
                }
            }
            System.out.println();
     
        }
    }
}

