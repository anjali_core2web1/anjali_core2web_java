class CharDemo {
	public static void main(String[] args)
	{
		char ch = 'F';
		
		if (ch < 90)
		{
			System.out.println(ch + " "+ "is uppercase character");
		}
		else {
			System.out.println(ch +" " + "is a lowercase character");
		}
	}
}
