import java.util.*;

class StringDemo4{
 	public static void main(String[] args)
	{	
		System.out.println("Enter String:");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		
		for(int i =0; i<str.length() ; i++)
		{
			char ch = str.charAt(i);
			System.out.println(ch);
		}
 }
}
