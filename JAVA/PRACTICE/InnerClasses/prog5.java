
class Outer4{

	Outer4(){
		System.out.println("Outer constructor");

	}

	class Inner{
	
		Inner(){
		
			System.out.println("Inner constructor");
		}
	}

	public static void main(String [] args){
	
		Outer4 obj1 = new Outer4();
		Outer4 obj2 = new Outer4();

		Inner obj = obj1.new Inner();
	}
}

