
class Outer5{

	int x =10;
	 static int y =20;

	 Outer5(){
	 	System.out.println("Outer constructor");
	 }

	 class Inner{
	 
	 	Inner(){
		
			System.out.println("Inner constructor");
			System.out.println(x);
			System.out.println(y);
			
		}
	 }
	 public static void main(String [] args){
	 
	 	Outer5 obj1 = new Outer5();
		Inner obj = obj1.new Inner();
	 }
}
