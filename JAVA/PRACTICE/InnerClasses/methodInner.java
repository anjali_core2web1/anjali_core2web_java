

class Outer{


	void fun(){

		System.out.println("in fun");
	
		class Inner1{
		
			Inner1(){
			
				System.out.println("Inner1- constructor");
			}

			void run(){

				System.out.println("In run");
			
				class Inner2{
				
					Inner2(){
					
						System.out.println("Inner2- constructor");

					}

				}

				Inner2 obj = new Inner2();
			
			}
		}

		Inner1 obj = new Inner1();
		obj.run();
		
		
	}
	

	public static void main(String [] args){
	
		Outer obj = new Outer();
		obj.fun();
		
}
}
