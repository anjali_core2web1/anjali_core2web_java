
class Outer6{

	int x =10;
	 static int y =20;

	 Outer6(){
	 	System.out.println("Outer constructor");
	 }

	 class Inner{
	 	int x=30;
	 	Inner(int x){
		
			System.out.println("Inner constructor");
			System.out.println(this.x);
			System.out.println(y);
			System.out.println(x);
			System.out.println(Outer6.this.x);
			
		}
	 }
	 public static void main(String [] args){
	 
	 	Outer6 obj1 = new Outer6();
		Inner obj = obj1.new Inner(120);
	 }
}
