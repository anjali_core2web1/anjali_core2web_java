class Parent{


}
class Child{

}

class Demo{

	void fun(Parent obj){
	
		System.out.println("IN FUN - PARENT");

	}

	void fun(Child obj){
	
		System.out.println("IN FUN - CHILD");
	}
}

class Outer{

	public static void main(String [] args){
	
		Demo obj = new Demo();
		obj.fun(new Parent());
		obj.fun(new Child());
	}
}
