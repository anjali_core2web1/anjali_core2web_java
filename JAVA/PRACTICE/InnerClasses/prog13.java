

class Outer10{

	 static int x = 10;

	 int y =20;

	 static void run(){
	 	
		 System.out.println("In static method");
	 }

	 void fun(){
	 	 
		 System.out.println("In non-static method");

	 }

	 static class Inner{
	 
	 	Inner(){
		
			System.out.println(x);
			run();
			//fun();  *ERROR
			//System.out.println(y); *ERROR

		}


	 }
}

class Client{

	public static void main(String [] args)
	{
	
		Outer10.Inner obj = new Outer10.Inner();
	//	Outer.Inner obj = new Outer(). new Inner(); * ERROR = not dependent on Outer class
	      
	}
}
