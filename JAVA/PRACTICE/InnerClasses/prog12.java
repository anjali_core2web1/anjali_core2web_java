
class Outer9{

	static int x=10;

	static void run(){
		
		System.out.println("In static");
	}

	class Inner{
		
		int y =20;

		void fun(){
			System.out.println("In fun");
			System.out.println(x);
			run();
		}
	}
	public static void main(String [] args){
		
		Outer9 obj1 = new Outer9();
		Inner obj = obj1.new Inner();

		System.out.println(obj.y);
		obj.fun();
	}
}
