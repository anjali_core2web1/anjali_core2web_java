


import java.util.*;

class ArrayDemo3
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i = 0; i<arr.length;i++)
		{
			System.out.println("Enter value for element:");
			arr[i]= sc.nextInt();
		}

		System.out.println("Array elements are:");

		for(int i = 0; i<arr.length;i++)
		{
			System.out.println(arr[i]);
		}
	
	}
}
