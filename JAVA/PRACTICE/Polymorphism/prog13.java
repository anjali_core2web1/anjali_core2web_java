

class Demo{

	 static int x=5;

	 static{
	 
	 	System.out.println("static block");
	 	System.out.println(x);
		
	 }

	 static void fun(){
	 	
		 System.out.println("static method");
	 	

	 }
	 
	 public static void main(String [] args){            /*program flow:
	 							
								1. static block 2. static variable 3. static method */
							       
		System.out.println("In main");
		fun();                                      
	 
	 }
}
