class Parent {

	int fun(){
	
		return 10;
	}
}

class Child extends Parent{
	
	long fun(){
	
		return 10L; // error : return type not compatible
	}
}
