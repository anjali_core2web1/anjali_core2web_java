class Parent {

	String fun(){
	
		return "Anju";
	}
}

class Child extends Parent{
	
	StringBuffer fun(){

		return new StringBuffer("shruti"); // error: returntype not compatible
	
	}
}
