

class Parent{


	static void fun(){
	
		System.out.println("in fun - parent");
	}
}

class Child extends Parent{
	void fun(){
		System.out.println("In fun - child");
	}

}

class Client{

	public static void main(String [] args){
		
		Child obj =new Child();
		obj.fun();
			
		
	}
}
