class SpacePattern {
    public static void main(String[] args) {
        int rowCount = 3; // Number of rows in the pattern
        int startNum = 1; // Starting number
        
        // Loop through each row
        for (int i = 0; i < rowCount; i++) {
            // Printing underscores and numbers
            for (int j = 0; j <= i; j++) {
                if (j > 0) {
                    System.out.print("  "); // Print spaces if not the first number in the row
                }
                System.out.print(startNum);
                startNum += 2; // Increment by 2 for the next number
            }
            System.out.println(); // Move to the next line after each row
        }
    }
}

