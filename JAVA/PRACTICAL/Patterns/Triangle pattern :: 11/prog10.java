import java.util.*;

class TriPattern10
{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int ch = 65;
		for(int i=1; i<=rows; i++){

			int num = 0;
			for(int j=rows; j>=i; j--){

				if((j%2 == 0 && i%2 == 1) || (j%2 == 1 && i%2 == 0)){
					System.out.print(ch + " ");
				}else{ 	
					System.out.print((char)ch + " ");
				}
				ch++;
				num++;
			}
			System.out.println();
			ch -= num - 1;
		}
	}
}
