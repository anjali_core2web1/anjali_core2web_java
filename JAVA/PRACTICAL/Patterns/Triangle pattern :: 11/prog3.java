import java.util.*;

class TriPattern3
{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		char ch = 'A';
		for(int i=1; i<=rows; i++){
			
			for(int j=1; j<=i; j++){

				System.out.print(ch++ + " ");
			}
			System.out.println();
			ch -= i-1;	
		}
	}
}
