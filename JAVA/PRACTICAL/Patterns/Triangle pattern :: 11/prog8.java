import java.util.*;

class TriPattern8
{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Rows = ");
                int rows = sc.nextInt();

		int num = 1;
                for(int i =1; i<=rows; i++){

			int cnt = 0;	
			for(int j=rows; j>=i; j--){

				System.out.print(num++ + " ");
				cnt++;
			}
			System.out.println();
			num -= cnt-1;
                }
        }
}
