import java.util.*;

class TriPattern6
{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.print("Rows = ");
		int rows = sc.nextInt();

		int num = rows;
		for(int i =1; i<=rows; i++){

			for(int j=rows; j>=i; j--){

				System.out.print(num + " ");
			}
			System.out.println();
			num--;
		}
	}
}
