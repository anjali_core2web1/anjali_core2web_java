import java.util.*;
class CharOcc
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Array Elements:");
		for(int i=0; i<size;i++)
		{
			arr[i] = sc.next().charAt(0);

		}
		
		System.out.println("Enter Character to be searched: ");
			char src = sc.next().charAt(0);
		int cnt=0;
		for(int i=0;i<size;i++)
		{
			
			if(arr[i]==src)
			{
				cnt++;
			}
			
			
		}

			System.out.println("Count of given char: " + cnt);
	}
}
