
import java.io.*;

class Oddproduct
{
	public static void main(String[] args) throws IOException
	
{
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		
		int arr [] = new int[size];

		System.out.println("Enter array elements:");
	       for(int i =0; i< size;i++)
	       {
	       	arr[i]=Integer.parseInt(br.readLine());
	       }

	       int prod =1 ;
	      
	       for(int j = 0; j<size;j++)
	       {
	       		if(j % 2 == 1)
			{
				prod*=arr[j];
			
				
			}
	       }
	       System.out.println("product of odd elements: "+ prod);

}	



}
