import java.util.*;
class Average
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");
		for(int i=0; i<size;i++)
		{
			arr[i] = sc.nextInt();

		}
		int sum=0;
		for(int j=0;j<size;j++)
		{
			sum+=arr[j];
		}
			int avg = sum/size;
			System.out.println("Array elements average: "+ avg);
	}
}
