import java.util.*;
class OccArray
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");
		for(int i=0; i<size;i++)
		{
			arr[i] = sc.nextInt();

		}
		System.out.println("enter num to check occurance:");
		int num = sc.nextInt();

		int cnt=0;
		for(int j=0;j<size;j++)
		{
			if(arr[j]==num)
			{
				cnt++;
			}
		}
		
		if(cnt == 2)
		{
			System.out.println("Number occurs 2 times !!");
		}
		else{
			System.out.println("Number occurs more then 2 times");
	}
	}
}
