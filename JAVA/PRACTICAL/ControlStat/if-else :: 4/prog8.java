class IfelseDemo7
{
	public static void main(String[] args)
	{
		double  mks = 26.00;
		
		
		if (mks>= 90 && mks <=100)
		{
			System.out.println("Passed with distinction");
		}
		
		else if (mks>= 80 && mks <=90)
		{
			System.out.println("Passed with First Class");
		}
		else if (mks>= 70 && mks <=80)
		{
			System.out.println("Passed with Second Class");
		}
		else if (mks>= 60 && mks <=70)
		{
			System.out.println("Passed with Satisfactory");
		}
		else if (mks>= 50 && mks <=60)
		{
			System.out.println("Passed with Fair");
		}
		 else if (mks>= 40 && mks <=50)
		{
			System.out.println("Passed But Needs Improvement");
		}
		else{
			System.out.println("Failed!! Better Luck Next Time");
		}
	}
}
