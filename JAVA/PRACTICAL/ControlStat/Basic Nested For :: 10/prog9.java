class forDemo9
{

        public static void main(String[] args){

                System.out.println("Number of rows = 4");
		int num = 1;
                for(int i=1; i<=4; i++){

                        for(int j=1; j<=4; j++){

                                System.out.print(num+" ");
				num++;
                        }
                        System.out.println();
			num--;
                }

                System.out.println("Number of rows = 3");
                num = 1; 
		for(int i=1; i<=3; i++){

                        for(int j=1; j<=3; j++){

                                System.out.print(num+" ");
				num++;
                        }
                        System.out.println();
			num--;
                }
        }
}
