class Demo9
{
	public static void main(String[] args){
		int num = 150;
		int sum = 0;
		while(num > 100){
			if(num % 2 ==1){
				sum += num;
			}
			num--;
		}
		System.out.println(sum);
	}
}
